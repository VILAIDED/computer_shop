package com.vilaided.webshop.security;

import com.vilaided.webshop.security.filters.JwtTokenFilter;
import com.vilaided.webshop.security.filters.JwtTokenFilterConfigurer;
import com.vilaided.webshop.security.filters.JwtUsernameAndPasswordAuthenticationFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfig  extends WebSecurityConfigurerAdapter {
    private final JwtTokenProvider jwtTokenProvider;
    @Autowired
    private AuthEntryPoint authEntryPoint;
    @Autowired
    private AccessDeniedCustom accessDeniedCustom;

    public WebSecurityConfig(JwtTokenProvider jwtTokenProvider) {
        this.jwtTokenProvider = jwtTokenProvider;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {

        // Disable CSRF (cross site request forgery)
        http.csrf().disable();

        // No session will be created or used by spring security
        http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
        http.exceptionHandling().authenticationEntryPoint(authEntryPoint).accessDeniedHandler(accessDeniedCustom);


        // Entry points
        http.authorizeRequests()//
                .antMatchers("/auth/register").permitAll()//
                .antMatchers("/auth/login").permitAll()//
                .antMatchers(HttpMethod.GET,"/brands/**").permitAll()
                .antMatchers(HttpMethod.PUT,"/brands/**").hasAuthority("ROLE_ADMIN")
                .antMatchers(HttpMethod.POST,"/brands/**").hasAuthority("ROLE_ADMIN")
                .antMatchers(HttpMethod.GET,"/computers/**").permitAll()
                .antMatchers(HttpMethod.POST,"/computers/**").hasAuthority("ROLE_ADMIN")
                .antMatchers(HttpMethod.POST,"/computers/**").hasAuthority("ROLE_ADMIN")
                // Disallow everything else..
                .anyRequest().authenticated();

        http.addFilter(new JwtUsernameAndPasswordAuthenticationFilter(authenticationManager(),jwtTokenProvider));
        http.addFilterBefore(new JwtTokenFilter(jwtTokenProvider), UsernamePasswordAuthenticationFilter.class);

    }



    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder(12);
    }

    @Override
    @Bean
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }
}
