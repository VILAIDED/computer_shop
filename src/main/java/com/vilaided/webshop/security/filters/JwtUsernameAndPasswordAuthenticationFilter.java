package com.vilaided.webshop.security.filters;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.vilaided.webshop.model.User;
import com.vilaided.webshop.payload.response.ResponseCustom;
import com.vilaided.webshop.security.JwtTokenProvider;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;

public class JwtUsernameAndPasswordAuthenticationFilter extends UsernamePasswordAuthenticationFilter {


        private final AuthenticationManager authenticationManager;
        private final JwtTokenProvider jwtTokenProvider;

    public JwtUsernameAndPasswordAuthenticationFilter(AuthenticationManager authenticationManager, JwtTokenProvider jwtTokenProvider) {
        this.authenticationManager = authenticationManager;
        this.jwtTokenProvider = jwtTokenProvider;
        this.setRequiresAuthenticationRequestMatcher(new AntPathRequestMatcher("/auth/login", "POST"));
    }


    @Override
        public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException {
            try {
            User user =new ObjectMapper().readValue(request.getInputStream(),User.class);
                Authentication authentication = new UsernamePasswordAuthenticationToken(user.getUserName(), user.getPassword());

                return authenticationManager.authenticate(authentication);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
        @Override
        protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain,
                                                Authentication auth) throws IOException, ServletException {
            String token = jwtTokenProvider.createToken(auth.getName());
            HashMap<String, String> tokenJson = new HashMap<String, String>();
            System.out.println("hello from auth" + token);


                tokenJson.put("token", token);
                ResponseCustom res = new ResponseCustom("OK", "", tokenJson);
                response.addHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
                response.getWriter().write(new ObjectMapper().writeValueAsString(res));
                response.setHeader("Authorization", token);
                response.setStatus(HttpStatus.OK.value());

        }
    }
