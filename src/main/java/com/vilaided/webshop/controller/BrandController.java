package com.vilaided.webshop.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.vilaided.webshop.mapper.BrandMapper;
import com.vilaided.webshop.model.Brand;
import com.vilaided.webshop.payload.response.ResponseCustom;
import com.vilaided.webshop.service.BrandQueryService;

@RestController
@RequestMapping(path = "/brands")

public class BrandController {
	
	private BrandQueryService brandQueryService;
	
	
	public BrandController(BrandQueryService brandQueryService, BrandMapper brandMapper) {
		super();
		this.brandQueryService = brandQueryService;
		
	}
	@PostMapping
	public ResponseEntity<Brand> createBrand(@Valid @RequestBody Brand brand) {
		int id = brandQueryService.insert(brand);
		Brand newBrand = brandQueryService.findById(id);
		return ResponseEntity.ok(newBrand);
	}
	@GetMapping
	public ResponseEntity<ResponseCustom<Optional<List<Brand>>>> getAllBrand(){
		ResponseCustom<Optional<List<Brand>>> res = new ResponseCustom<Optional<List<Brand>>>();
		res.setStatus("Ok");
		res.setData(brandQueryService.finAll());
		return ResponseEntity.ok(res);
	}
	
	
}
