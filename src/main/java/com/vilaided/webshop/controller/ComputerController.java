package com.vilaided.webshop.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import com.vilaided.webshop.payload.request.ComputerRequest;
import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.vilaided.webshop.model.Component;
import com.vilaided.webshop.model.Computer;
import com.vilaided.webshop.model.ComputerData;
import com.vilaided.webshop.payload.response.ResponseCustom;
import com.vilaided.webshop.service.ComponentQueryService;
import com.vilaided.webshop.service.ComputerQueryService;

@RestController
@RequestMapping(path= "/computers")
@AllArgsConstructor
public class ComputerController {
	private ComputerQueryService computerQueryService;
	private ComponentQueryService componentQueryService;

	

	@GetMapping(path= "{id}")
	public ResponseEntity<ResponseCustom>  findById(@PathVariable("id") String id){
		return ResponseEntity.ok(new ResponseCustom("OK","",computerQueryService.findById(id)));
	}
	@GetMapping
	public ResponseEntity<ResponseCustom<Optional<List<ComputerData>>>> findAll(){
		ResponseCustom<Optional<List<ComputerData>>> response = new ResponseCustom<Optional<List<ComputerData>>>("OK","",computerQueryService.findAll());
		return ResponseEntity.ok(response);
	}
	@PostMapping
	public ResponseEntity createComputer(@Valid @RequestBody ComputerRequest computerRequest) {
	
		int id = computerQueryService.insert(computerRequest.getComputer());
		if(id > 0) {
			Component component = computerRequest.getComponent();
			component.setComputerId(id);
			componentQueryService.insert(component);
			return ResponseEntity.ok(new ResponseCustom("OK","",computerQueryService.findById(Integer.toString(id))));
		}else {
			return new ResponseEntity(new ResponseCustom("FAILTED","brand with id = " + computerRequest.getComputer().getBrandId() + " is not exist"),HttpStatus.BAD_REQUEST);
		}
	}
	@PutMapping(path="{id}")
	public ResponseCustom updateComputer(@PathVariable("id")String id, @Valid @RequestBody ComputerRequest computerRequest) {
		if(computerQueryService.findById(id) == null) {
			return new ResponseCustom("FAILED","Can't not find Computer with  this id : " + id);
		}
		Computer computer = computerRequest.getComputer();
		computer.setId(Integer.parseInt(id));
		int check = computerQueryService.update(computer);
		if(check > 0) {
			componentQueryService.update(computerRequest.getComponent());
			return new ResponseCustom("OK","",computerQueryService.findById(id));
		}
		return new ResponseCustom("OK","",computerQueryService.findById(id));
	}
  }  


