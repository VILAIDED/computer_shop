package com.vilaided.webshop.controller;

import javax.validation.Valid;

import com.vilaided.webshop.payload.request.SignupRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.vilaided.webshop.payload.response.ResponseCustom;
import com.vilaided.webshop.model.User;
import com.vilaided.webshop.service.UserService;

@RestController
@RequestMapping(path= "/auth")
public class AuthController {
	private UserService userService;
	@Autowired    
	private BCryptPasswordEncoder bCryptPasswordEncoder;
	public AuthController(UserService userService) {
		super();
		this.userService = userService;
	}
	@PostMapping(path="/register")
	public ResponseEntity register(@Valid @RequestBody SignupRequest req) {
	return userService.Register(req);
	}
	
	
}
