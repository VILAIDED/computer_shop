package com.vilaided.webshop.controller;

import javax.validation.Valid;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.vilaided.webshop.model.Component;
import com.vilaided.webshop.payload.response.ResponseCustom;
import com.vilaided.webshop.service.ComponentQueryService;

@RestController
@RequestMapping(path="/components")
public class ComponentController {
	private ComponentQueryService componentQueryService;

	public ComponentController(ComponentQueryService componentQueryService) {
		super();
		this.componentQueryService = componentQueryService;
	}
	@GetMapping(path="{id}")
	public ResponseCustom findById(@PathVariable("id") String id) {
		return new ResponseCustom("OK","",componentQueryService.findByComputerId(id));
	}
	@PostMapping
	public ResponseCustom createComponent(@RequestBody @Valid Component component) {
		int id = componentQueryService.insert(component);
		if(id > 0) {
			return new ResponseCustom("OK","",componentQueryService.findByComputerId(Long.toString(component.getComputerId())));
		}else {
			return new ResponseCustom("FAILED","something went wrong");
		}
	}
}
