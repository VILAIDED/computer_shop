package com.vilaided.webshop.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ComputerData {
	private long id;
	private String nameLaptop;
	private String price;
	private String info;
	private int status;
	private int stock;
	@JsonProperty("brand")
	private Brand brand;
	
	@JsonProperty("component")
	private Component component;
	public ComputerData(long id, String nameLaptop, String price, String info, int status, int stock, Brand brand,
			Component component) {
		super();
		this.id = id;
		this.nameLaptop = nameLaptop;
		this.price = price;
		this.info = info;
		this.status = status;
		this.stock = stock;
		this.brand = brand;
		this.component = component;
	}

	public Component getComponent() {
		return component;
	}

	public void setComponent(Component component) {
		this.component = component;
	}

	public ComputerData() {
		super();
	}
	

	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getNameLaptop() {
		return nameLaptop;
	}
	public void setNameLaptop(String nameLaptop) {
		this.nameLaptop = nameLaptop;
	}
	public String getPrice() {
		return price;
	}
	public void setPrice(String price) {
		this.price = price;
	}
	public String getInfo() {
		return info;
	}
	public void setInfo(String info) {
		this.info = info;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public int getStock() {
		return stock;
	}
	public void setStock(int stock) {
		this.stock = stock;
	}
	public Brand getBrand() {
		return brand;
	}
	public void setBrand(Brand brand) {
		this.brand = brand;
	}
	
	
	
}	
