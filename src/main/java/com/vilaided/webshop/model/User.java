package com.vilaided.webshop.model;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@Table(name="users")
public class User {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@Column(name="user_name")
	private String username;

	@JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
	@Column(name="pw")
	private String password;
	@ManyToOne
	@JoinColumn(name="role_id", nullable = false)
	private Role role;
	public User(){

	}
	public User(long id, String userName, String password, Role role) {
		this.id = id;
		this.username = userName;
		this.password = password;
		this.role = role;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public User(@JsonProperty("username") String userName, @JsonProperty("password") String password) {
		this.username = userName;
		this.password = password;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public void setUserName(String userName) {
		this.username = userName;
	}

	public void setPassword(String password) {
		this.password = password;
	}


	public String getUserName() {
		return username;
	}

	public String getPassword() {
		return password;
	}
}

