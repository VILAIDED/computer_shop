package com.vilaided.webshop.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="computers")
public class Computer {
	@Id
	@GeneratedValue(strategy= GenerationType.AUTO)
	private long id;
	
	@Column(name="brand_id")
	private long brandId;
	
	@Column(name="name_laptop")
	private String nameLaptop;
	
	@Column(name= "price")
	private String price;
	
	@Column(name="info")
	private String info;
	
	@Column(name="computer_status")
	private int status;
	
	@Column(name="stock")
	private Integer stock;
	
	

	public Computer(long id, long brandId, String nameLaptop, String price, String info, int status, Integer stock) {
		super();
		this.id = id;
		this.brandId = brandId;
		this.nameLaptop = nameLaptop;
		this.price = price;
		this.info = info;
		this.status = status;
		this.stock = stock;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getBrandId() {
		return brandId;
	}

	public void setBrandId(long brandId) {
		this.brandId = brandId;
	}

	public String getNameLaptop() {
		return nameLaptop;
	}

	public void setNameLaptop(String nameLaptop) {
		this.nameLaptop = nameLaptop;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public Integer getStock() {
		return stock;
	}

	public void setStock(Integer stock) {
		this.stock = stock;
	}
	
	
	
}
