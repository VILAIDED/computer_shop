package com.vilaided.webshop.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@Entity 
@Table(name="components")
public class Component {
	@Id
	@GeneratedValue(strategy =GenerationType.AUTO)
	private long id;
	
	@JsonInclude(Include.NON_DEFAULT)
	@Column(name="computer_id")
	private long computerId;
	
	@Column(name="type_cpu")
	private String typeCpu;
	
	@Column(name="type_disk")
	private String typeDisk;
	
	@Column(name="size_screen")
	private Double sizeScreen;
	
	@Column(name="size_disk")
	private Double sizeDisk;

	
	@Column(name="size_ram")
	private Double sizeRam;
	
	@Column(name="size_pin")
	private Double sizePin;
	
	@Column(name="weight")
	private Double weight;
	
	
	public Component() {
		
	}
	public Component(long id, long computerId, String typeCpu, Double sizeScreen, Double sizeDisk, String typeDisk,
			Double sizeRam, Double sizePin, Double weight) {
		super();
		this.id = id;
		this.computerId = computerId;
		this.typeCpu = typeCpu;
		this.sizeScreen = sizeScreen;
		this.sizeDisk = sizeDisk;
		this.typeDisk = typeDisk;
		this.sizeRam = sizeRam;
		this.sizePin = sizePin;
		this.weight = weight;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getComputerId() {
		return computerId;
	}

	public void setComputerId(long computerId) {
		this.computerId = computerId;
	}

	public String getTypeCpu() {
		return typeCpu;
	}

	public void setTypeCpu(String typeCpu) {
		this.typeCpu = typeCpu;
	}

	public Double getSizeScreen() {
		return sizeScreen;
	}

	public void setSizeScreen(Double sizeScreen) {
		this.sizeScreen = sizeScreen;
	}

	public Double getSizeDisk() {
		return sizeDisk;
	}

	public void setSizeDisk(Double sizeDisk) {
		this.sizeDisk = sizeDisk;
	}

	public String getTypeDisk() {
		return typeDisk;
	}

	public void setTypeDisk(String typeDisk) {
		this.typeDisk = typeDisk;
	}

	public Double getSizeRam() {
		return sizeRam;
	}

	public void setSizeRam(Double sizeRam) {
		this.sizeRam = sizeRam;
	}

	public Double getSizePin() {
		return sizePin;
	}

	public void setSizePin(Double sizePin) {
		this.sizePin = sizePin;
	}

	public Double getWeight() {
		return weight;
	}

	public void setWeight(Double weight) {
		this.weight = weight;
	}
	
	
}
