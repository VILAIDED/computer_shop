package com.vilaided.webshop.mapper;

import com.vilaided.webshop.model.Role;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

@Mapper
public interface RoleMapper {
    @Select("SELECT * FROM roles WHERE id = #{id}")
    Role findById(@Param("id") Long id);

}
