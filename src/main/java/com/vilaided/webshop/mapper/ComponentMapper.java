package com.vilaided.webshop.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.vilaided.webshop.model.Component;

@Mapper
public interface ComponentMapper {
	int insert(@Param("component") Component component);
	int update(@Param("component") Component component);
	Component findByComputerId(@Param("computerId") String computerId);
	
}
