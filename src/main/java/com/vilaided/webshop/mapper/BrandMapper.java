package com.vilaided.webshop.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.vilaided.webshop.model.Brand;

@Mapper
public interface BrandMapper {
	int insert(@Param("brand") Brand brand);
	
	int update(@Param("brand") Brand brand);
	
	int delete(@Param("id") String id);
	
	List<Brand> findAll();
	Brand findById(@Param("id") String id);
	
}
