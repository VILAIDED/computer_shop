package com.vilaided.webshop.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.vilaided.webshop.model.Computer;
import com.vilaided.webshop.model.ComputerData;

@Mapper
public interface ComputerMapper {
	int insert(@Param("computer") Computer computer);
	
	int delete(@Param("id") String id);
	
	List<Computer> findByBrandId(@Param("brandId") String brandId);
	
	List<Computer> findByStatus(@Param("status") String status);
	
	ComputerData findById(@Param("id") String id);
	
	List<ComputerData> findAll();
	
	int update(@Param("computer") Computer computer);
}
