package com.vilaided.webshop.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.vilaided.webshop.model.User;

@Mapper
public interface UserMapper {
	
	int insert(@Param("user") User user);
	int update(@Param("user") User user);
	User findByName(@Param("name") String name);
	User findById(@Param("id") String id);
}
