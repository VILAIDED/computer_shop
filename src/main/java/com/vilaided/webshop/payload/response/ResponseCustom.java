package com.vilaided.webshop.payload.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

public class ResponseCustom<T> {
	
	private String status;
	@JsonInclude(Include.NON_EMPTY)
	private String message;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private T data;
	
	public ResponseCustom() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public ResponseCustom(String status, String message, T data) {
		super();
		this.status = status;
		this.message = message;
		this.data = data;
	}
	

	public ResponseCustom(String status, String message) {
		super();
		this.status = status;
		this.message = message;
	}

	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public T getData() {
		return data;
	}
	public void setData(T data) {
		this.data = data;
	}
	
}
