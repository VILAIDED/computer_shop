package com.vilaided.webshop.payload.request;

import com.vilaided.webshop.model.Component;
import com.vilaided.webshop.model.Computer;

public class ComputerRequest{
    private Computer computer;
    private Component component;

    public ComputerRequest(Computer computer, Component component) {
        super();
        this.computer = computer;
        this.component = component;
    }
    public Computer getComputer() {
        return computer;
    }
    public void setComputer(Computer computer) {
        this.computer = computer;
    }
    public Component getComponent() {
        return component;
    }
    public void setComponent(Component component) {
        this.component = component;
    }


}