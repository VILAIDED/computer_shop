package com.vilaided.webshop.service;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import com.vilaided.webshop.mapper.BrandMapper;
import com.vilaided.webshop.model.Brand;

@Service
public class BrandQueryService {
	private BrandMapper brandMapper;
	public BrandQueryService(BrandMapper brandMapper) {
		super();
		this.brandMapper = brandMapper;
	}
	public Optional<List<Brand>> finAll(){
		List<Brand> brands = brandMapper.findAll();
		if(brands == null) {
			return Optional.empty();
		}
		return Optional.of(brands);
	}
	public int insert(Brand brand) {
		return brandMapper.insert(brand);
	}
	public Brand findById(int id) {
		return brandMapper.findById(Integer.toString(id));
	}
	public int update(Brand brand) {
		return brandMapper.update(brand);
	}
	public int delete(String id) {
		return brandMapper.delete(id);
	}

}
