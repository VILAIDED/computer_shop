package com.vilaided.webshop.service;

import com.vilaided.webshop.exception.CustomException;
import com.vilaided.webshop.mapper.RoleMapper;
import com.vilaided.webshop.model.Role;
import com.vilaided.webshop.payload.request.SignupRequest;
import com.vilaided.webshop.payload.response.ResponseCustom;
import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.vilaided.webshop.mapper.UserMapper;
import com.vilaided.webshop.model.User;

@Service
@AllArgsConstructor
public class UserService {
	private UserMapper userMapper;
	private RoleMapper roleMapper;
	private final PasswordEncoder passwordEncoder;

	public User findById(String id) {
		return userMapper.findById(id);
	}
	public ResponseEntity Register(SignupRequest req) {
		if (userMapper.findByName(req.getUsername()) == null) {
			User user = new User();
			user.setUserName(req.getUsername());
			user.setPassword(passwordEncoder.encode(req.getPassword()));
			Role role = roleMapper.findById(req.getRoleId());
			System.out.println("roel" + role.getId());
			user.setRole(role);
			userMapper.insert(user);
			return new ResponseEntity(new ResponseCustom<>("OK","",user),HttpStatus.CREATED);
		} else {
			return new ResponseEntity(new ResponseCustom<>("FAILED","Username : " + req.getUsername() + " is already exist"),HttpStatus.UNPROCESSABLE_ENTITY);

		}
	}
	
}
