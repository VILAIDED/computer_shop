package com.vilaided.webshop.service;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import com.vilaided.webshop.mapper.BrandMapper;
import com.vilaided.webshop.mapper.ComputerMapper;
import com.vilaided.webshop.model.Computer;
import com.vilaided.webshop.model.ComputerData;

@Service

public class ComputerQueryService {
	private ComputerMapper computerMapper;
	private BrandMapper brandMapper;
	
	public ComputerQueryService(ComputerMapper computerMapper,BrandMapper brandMapper) {
		super();
		this.computerMapper = computerMapper;
		this.brandMapper = brandMapper;
	}
	public int insert(Computer computer) {
		int id;
		if(brandMapper.findById(Long.toString(computer.getBrandId()))== null) {
			return 0;
		}
		if(computerMapper.findById(Long.toString(computer.getId())) == null) {
			computerMapper.insert(computer);
			
			System.out.println("insert" + computer.getId());
		}
		else {
			System.out.println("update");
			computerMapper.update(computer);
		}
		return (int)computer.getId();

	}
	public int update(Computer computer) {
		return computerMapper.update(computer);
	}
	public int delete(String id) {
		return computerMapper.delete(id);
	}
	public Optional<ComputerData> findById(String id){
		ComputerData computer = computerMapper.findById(id);
		if(computer == null) {
			return Optional.empty();
		}
		return Optional.of(computer);
	}
	public Optional<List<ComputerData>> findAll(){
		List<ComputerData> computer = computerMapper.findAll();
		if(computer == null) {
			return Optional.empty();
		}
		return Optional.of(computer);
	}
	public Optional<List<Computer>> findByBrandId(String id){
		List<Computer> computer = computerMapper.findByBrandId(id);
		if(computer == null) {
			return Optional.empty();
		}
		return Optional.of(computer);
	}
	public Optional<List<Computer>> findByStatus(String id){
		List<Computer> computer = computerMapper.findByStatus(id);
		if(computer == null) {
			return Optional.empty();
		}
		return Optional.of(computer);
	}
	
}
