package com.vilaided.webshop.service;

import java.util.Optional;

import org.springframework.stereotype.Service;

import com.vilaided.webshop.mapper.ComponentMapper;
import com.vilaided.webshop.model.Component;

@Service
public class ComponentQueryService {
	private ComponentMapper componentMapper;
	
	public ComponentQueryService(ComponentMapper componentMapper) {
		super();
		this.componentMapper = componentMapper;
	}
	public int insert(Component component) {
		if(componentMapper.findByComputerId(Long.toString(component.getComputerId())) == null) {
			return componentMapper.insert(component);
		}else {
			return componentMapper.update(component);
		}
	}
	public int update(Component component) {
		return componentMapper.update(component);
	}
	public Optional<Component> findByComputerId(String computerId){
		Component component = componentMapper.findByComputerId(computerId);
		if(component != null) {
			return Optional.of(component);
		}else {
			return Optional.empty();
		}
	}
}
